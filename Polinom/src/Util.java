import monom.Monom;
import monom.MonomIntreg;
import polinom.Polinom;

public class Util {

	public Polinom parsePolinom(String polString)
	{
		Polinom rezultat = new Polinom();
		
		String[] splitedPol = polString.split("[-+]");
		
		for(int i=0; i<splitedPol.length; i++) {	
			String mon = splitedPol[i];
			if (mon.length() == 0) {
				continue;
			}
			String sign = "+";
			if (i > 0) {
				sign = polString.substring(polString.indexOf(splitedPol[i])-1, polString.indexOf(splitedPol[i]));
			}
			
			String[] monSplitted = mon.split("[X|x^]");
			if (monSplitted[0].length() >0) {
				Integer coef = Integer.parseInt(sign + monSplitted[0]);
				Integer grad = (monSplitted.length == 1) ? 0 : Integer.parseInt(monSplitted[monSplitted.length -1]);
				Monom monom = new MonomIntreg(coef, grad);
				rezultat.addMonom(monom);
			}
		}
		
		return rezultat;
	}
}
