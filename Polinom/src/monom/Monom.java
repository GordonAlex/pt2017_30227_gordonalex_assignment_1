package monom;
public abstract class Monom implements Cloneable{
		
	protected Number coef;
	protected Integer grad;
	
	public Monom(Number coef, Integer grad)
	{
		this.coef = coef;
		this.grad = grad;
	}
	
	public abstract Number getCoef();
	
	public abstract String getCoefSign();
	
	public abstract void addCoefConst(Number value);
	
	public abstract Monom mulCoefConst(Number value);
	
	public abstract Monom divCoefConst(Number value);
	
	public abstract Monom addMultipliedValue(Number coef, Integer grad);
	
	public abstract Monom addDividedValue(Number coef, Integer grad);
	
	public abstract Monom oposite();
	
	
	public void setCoef(Number coef)
	{
		this.coef = coef;
	}
	
	public Integer getGrad()
	{
		return this.grad;
	}
	
	public void setGrad(Integer grad)
	{
		this.grad = grad;
	}
	
	public String toString()
	{
		String s = "";
		
		if (this.coef.equals(0)) {
			return s;
		}
		
		s = this.getCoefSign() + this.coef + "X" + "^" + this.grad;
		return s;
	}
	
	public Object clone() {
        try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
}
