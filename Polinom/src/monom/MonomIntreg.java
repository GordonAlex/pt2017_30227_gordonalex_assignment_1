package monom;

public class MonomIntreg extends Monom{

	public MonomIntreg(Integer coef, Integer grad) {
		super(coef, grad);
	}

	@Override
	public Number getCoef() {
		return this.coef.intValue();
	}

	@Override
	public void addCoefConst(Number value) {
		Number newCoef = this.getCoef().intValue() + value.intValue();
		this.setCoef(newCoef);
	}

	@Override
	public Monom addMultipliedValue(Number coef, Integer grad) {
		Number newCoef = this.getCoef().intValue() * coef.intValue();
		Integer newGrad = this.getGrad() + grad;
		
		this.setCoef(newCoef);
		this.setGrad(newGrad);
		
		return this;
	}
	
	@Override
	public MonomIntreg mulCoefConst(Number value) {
		Integer newCoef = this.getCoef().intValue()*value.intValue();
		this.setCoef(newCoef);
		
		return this;
	}

	@Override
	public MonomIntreg divCoefConst(Number value) {
		Number newCoef = this.getCoef().intValue() / value.intValue();
		this.setCoef(newCoef);	
		
		return this;
	}
	
	@Override
	public Monom oposite() {
		Number newCoef = this.getCoef().intValue() * (-1);
		this.setCoef(newCoef);
		
		return this;
	}

	@Override
	public String getCoefSign() {
		if (this.getCoef().intValue() < 0) {
			return "";
		}
		
		return "+";
	}

	@Override
	public MonomIntreg addDividedValue(Number coef, Integer grad) {
		System.out.println(coef.intValue());
		Number newCoef = this.getCoef().intValue() / coef.intValue();
		this.setCoef(newCoef);
		
		
		Integer newGrad = this.getGrad().intValue() - grad.intValue();
		this.setGrad(newGrad);
		
		return this;
	}
	

}
