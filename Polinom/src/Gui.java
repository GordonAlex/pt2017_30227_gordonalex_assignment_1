import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import operations.Operations;
import polinom.Polinom;

public class Gui {

	public static void main(String[] args) {
		final JFrame frame = new JFrame();
		
		frame.setTitle("Polinom"); 
		frame.setVisible(true);
		frame.setSize(1000, 600);
		frame.setBackground(Color.DARK_GRAY);
		frame.setLayout(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//buttons
		JButton addButton = new JButton("Adunare");
		addButton.setVisible(true);
		addButton.setBounds(20, 50, 100, 30);
		frame.add(addButton);
		
		JButton subButton = new JButton("Scadere");
		subButton.setVisible(true);
		subButton.setBounds(20, 90, 100, 30);
		frame.add(subButton);
		
		JButton mulButton = new JButton("Inmultire");
		mulButton.setVisible(true);
		mulButton.setBounds(20, 130, 100, 30);
		frame.add(mulButton);
		
		JButton divButton = new JButton("Impartire");
		divButton.setVisible(true);
		divButton.setBounds(20, 170, 100, 30);
		frame.add(divButton);
		
		//polinom one label
		JLabel polOneLabel = new JLabel("Polinom:");
		polOneLabel.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		polOneLabel.setForeground(Color.black);		
		polOneLabel.setBounds(250, 30, 100, 20);
		polOneLabel.setVisible(true);
		frame.add(polOneLabel);
		
		//polinom one text input
		final JTextField polOneField = new JTextField();
		polOneField.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		polOneField.setBackground(Color.white);
		polOneField.setBounds(370, 25, 400, 30);
		polOneField.setVisible(true);
		frame.add(polOneField);
		
		//polinom two label
		JLabel polTwoLabel = new JLabel("Polinom:");
		polTwoLabel.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		polTwoLabel.setForeground(Color.black);		
		polTwoLabel.setBounds(250, 70, 100, 20);
		polTwoLabel.setVisible(true);
		frame.add(polTwoLabel);
		
		//polinom two text input
		final JTextField polTwoField = new JTextField();
		polTwoField.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		polTwoField.setBackground(Color.white);
		polTwoField.setBounds(370, 65, 400, 30);
		frame.add(polTwoField);
		
		//result one label
		JLabel resOneLabel = new JLabel("Rezultat:");
		resOneLabel.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		resOneLabel.setForeground(Color.black);		
		resOneLabel.setBounds(250, 120, 100, 20);
		resOneLabel.setVisible(true);
		frame.add(resOneLabel);
		
		//result one text input
		final JTextField resOneField = new JTextField();
		resOneField.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		resOneField.setBackground(Color.white);
		resOneField.setBounds(370, 115, 400, 30);
		resOneField.setVisible(true);
		frame.add(resOneField);

		JSeparator separator = new JSeparator();
		separator.setBounds(0, 250, 1400, 10);
	    frame.add(separator);
		
		JButton derButton = new JButton("Derivare");
		derButton.setVisible(true);
		derButton.setBounds(20, 270, 100, 30);
		frame.add(derButton);
		
		JButton intButton = new JButton("Integrare");
		intButton.setVisible(true);
		intButton.setBounds(20, 310, 100, 30);
		frame.add(intButton);
		
		//polinom label
		JLabel polLabel = new JLabel("Polinom:");
		polLabel.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		polLabel.setForeground(Color.black);		
		polLabel.setBounds(250, 270, 100, 20);
		polLabel.setVisible(true);
		frame.add(polLabel);
		
		//polinom text input
		final JTextField polField = new JTextField();
		polField.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		polField.setBackground(Color.white);
		polField.setBounds(370, 265, 400, 30);
		frame.add(polField);
		
		//result label
		JLabel resLabel = new JLabel("Rezultat:");
		resLabel.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		resLabel.setForeground(Color.black);		
		resLabel.setBounds(250, 320, 100, 20);
		resLabel.setVisible(true);
		frame.add(resLabel);
		
		//result one text input
		final JTextField resField = new JTextField();
		resField.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		resField.setBackground(Color.white);
		resField.setBounds(370, 315, 400, 30);
		resField.setVisible(true);
		frame.add(resField);
		
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try 
				{	
					String polOneString = polOneField.getText();
					String polTwoString = polTwoField.getText();
					
					if (polOneString.length() == 0 || polTwoString.length() == 0) {
						JOptionPane.showMessageDialog(frame, "Cel putin unul dintre field-urile polinom este gol!");
						return;
					}
					
					Util utils = new Util();
					Polinom p1 = utils.parsePolinom(polOneString);
					Polinom p2 = utils.parsePolinom(polTwoString);
					
					Operations op= new Operations();
					Polinom res = op.adunare(p1, p2);
					
					resOneField.setText(res.toString());
					
				} catch (Exception e) {
					JOptionPane.showMessageDialog(frame, "A aparut o eroare!");
				}				
			}
		});
		
		subButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try 
				{	
					String polOneString = polOneField.getText();
					String polTwoString = polTwoField.getText();
					
					if (polOneString.length() == 0 || polTwoString.length() == 0) {
						JOptionPane.showMessageDialog(frame, "Cel putin unul dintre field-urile polinom este gol!");
						return;
					}
					
					Util utils = new Util();
					Polinom p1 = utils.parsePolinom(polOneString);
					Polinom p2 = utils.parsePolinom(polTwoString);
					
					Operations op= new Operations();
					Polinom res = op.diferenta(p1, p2);
					
					resOneField.setText(res.toString());
					
				} catch (Exception e) {
					JOptionPane.showMessageDialog(frame, "A aparut o eroare!");
				}				
			}
		});
		
		mulButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try 
				{	
					String polOneString = polOneField.getText();
					String polTwoString = polTwoField.getText();
					
					if (polOneString.length() == 0 || polTwoString.length() == 0) {
						JOptionPane.showMessageDialog(frame, "Cel putin unul dintre field-urile polinom este gol!");
						return;
					}
					
					Util utils = new Util();
					Polinom p1 = utils.parsePolinom(polOneString);
					Polinom p2 = utils.parsePolinom(polTwoString);
					
					Operations op= new Operations();
					Polinom res = op.inmultire(p1, p2);
					
					resOneField.setText(res.toString());
					
				} catch (Exception e) {
					JOptionPane.showMessageDialog(frame, "A aparut o eroare!");
				}				
			}
		});
		
		divButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
//				try 
//				{	
					String polOneString = polOneField.getText();
					String polTwoString = polTwoField.getText();
					
					if (polOneString.length() == 0 || polTwoString.length() == 0) {
						JOptionPane.showMessageDialog(frame, "Cel putin unul dintre field-urile polinom este gol!");
						return;
					}
					
					Util utils = new Util();
					Polinom p1 = utils.parsePolinom(polOneString);
					Polinom p2 = utils.parsePolinom(polTwoString);
					
					Operations op= new Operations();
					Polinom res = op.impartire(p1, p2);
					
					resOneField.setText(res.toString());
					
//				} catch (Exception e) {
//					JOptionPane.showMessageDialog(frame, "A aparut o eroare!");
//				}				
			}
		});
		
		derButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try 
				{	
					String polString = polField.getText();
					
					if (polString.length() == 0) {
						JOptionPane.showMessageDialog(frame, "Field-ulpolinom este gol!");
						return;
					}
					
					Util utils = new Util();
					Polinom p1 = utils.parsePolinom(polString);
					
					Operations op= new Operations();
					Polinom res = op.derivare(p1);
					
					resField.setText(res.toString());
					
				} catch (Exception e) {
					JOptionPane.showMessageDialog(frame, "A aparut o eroare!");
				}				
			}
		});
		
		intButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try 
				{	
					String polString = polField.getText();
					
					if (polString.length() == 0) {
						JOptionPane.showMessageDialog(frame, "Field-ulpolinom este gol!");
						return;
					}
					
					Util utils = new Util();
					Polinom p1 = utils.parsePolinom(polString);
					
					Operations op= new Operations();
					Polinom res = op.integrare(p1);
					
					resField.setText(res.toString());
					
				} catch (Exception e) {
					JOptionPane.showMessageDialog(frame, "A aparut o eroare!");
				}				
			}
		});
	}

}
