package operations;
import monom.Monom;
import polinom.Polinom;

public class Operations {

	public Operations() {
		
	}
	
	public Polinom adunare(Polinom p1, Polinom p2) {
		Polinom rezultat = new Polinom();
		
		Integer gradPol1 = p1.getGrad();
		Integer gradPol2 = p2.getGrad();
		
		Integer gradMax = (gradPol1 > gradPol2) ? gradPol1 : gradPol2;
		
		for (int i=0;i<=gradMax;i++) {
			if (p1.getMonomFromPos(i) != null) {
				rezultat.addMonom(p1.getMonomFromPos(i));
			}
			
			if (p2.getMonomFromPos(i) != null) {
				rezultat.addMonom(p2.getMonomFromPos(i));
			}
		}
		
		return rezultat;
	}
	
	public Polinom diferenta(Polinom p1, Polinom p2) {
		Polinom rezultat = new Polinom();
		
		Integer gradPol1 = p1.getGrad();
		Integer gradPol2 = p2.getGrad();
		
		Integer gradMax = (gradPol1 > gradPol2) ? gradPol1 : gradPol2;
		
		for (int i=0;i<=gradMax;i++) {
			if (p1.getMonomFromPos(i) != null) {
				rezultat.addMonom(p1.getMonomFromPos(i));
			}
			
			if (p2.getMonomFromPos(i) != null) {
				Monom m = p2.getMonomFromPos(i);
				m.oposite();
				rezultat.addMonom(m);
			}
		}
		
		return rezultat;
	}
	
	public Polinom inmultire(Polinom p1, Polinom p2) {
		Polinom rezultat = new Polinom();
		
		for (Monom monom1 : p1.getMonoame()) {
			if (monom1 == null) {
				continue;
			}
			
			for (Monom monom2 : p2.getMonoame()) {
				if (monom2 == null) {
					continue;
				}
				Monom temp = (Monom) monom1.clone();
				temp.addMultipliedValue(monom2.getCoef(), monom2.getGrad());
				rezultat.addMonom(temp);
			}
		}
		return rezultat;
	}
	
	public Polinom impartire(Polinom p1,Polinom p2) {
		
		Polinom rest = new Polinom();
		Polinom cat = new Polinom();
		
		Polinom temp1 = new Polinom();
		Polinom temp2 = new Polinom();
		
		Monom m2 = p2.getMonomFromPos(p2.getGrad());
		temp2 = p1;
		
		while ( temp2.getGrad() !=0 ) {
			Monom m1 = temp2.getMonomFromPos(temp2.getGrad());
			Monom mt = m1.addDividedValue(m2.getCoef(), m2.getGrad());
			cat.addMonom(mt);
			Monom cn = mt.oposite();
			
			for (Monom monom2 : p2.getMonoame()) {
				if (monom2 == null) {
					continue;
				}
				temp1.addMonom(monom2.addMultipliedValue(cn.getCoef(), cn.getGrad()));
			}

			temp2 = this.adunare(p1, temp1);
			
		}
		rest = temp2;
		return cat;
	}

	public Polinom derivare(Polinom p) {
		Polinom rezultat = new Polinom();
		
		Monom mt1 = p.getMonomFromPos(p.getGrad());
		
		for(int i=0;i<=p.getGrad(); i++) {
			if (p.getMonomFromPos(i) != null) {
				mt1.setGrad(p.getGrad() - 1);
				mt1.mulCoefConst(p.getGrad());
				rezultat.addMonom(mt1);
			}
		}
		
		return rezultat;
	}
	
	public Polinom integrare(Polinom p) {
		Polinom rezultat = new Polinom();
		
		Monom mt1 = p.getMonomFromPos(p.getGrad());
		
		for(int i=0;i<=p.getGrad(); i++) {
			if (p.getMonomFromPos(i) != null) {
				mt1.setGrad(p.getGrad() + 1);
				mt1.divCoefConst(p.getGrad());
				
				rezultat.addMonom(mt1);
			}
		}
		
		return rezultat;
	}
}
