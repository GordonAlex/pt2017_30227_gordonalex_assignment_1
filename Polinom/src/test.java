import java.text.NumberFormat;

import monom.Monom;
import monom.MonomIntreg;

public class test {

	public static void main(String[] args) {
		String pol ="4X^7-2X^1-1X^0+7x^3";
		
		String[] splitedPol = pol.split("[-+]");
		for(int i=0; i<splitedPol.length; i++) {
			String mon = splitedPol[i];
			if (mon.length() == 0) {
				continue;
			}
			String sign = "+";
			if (i > 0) {
				sign = pol.substring(pol.indexOf(splitedPol[i])-1, pol.indexOf(splitedPol[i]));
			}
			String[] monSplitted = mon.split("[X|x^]");
			//for(int k=0;k<monSplitted.length;k++) {
			if (monSplitted[0].length() > 0) {
				Integer coef = Integer.parseInt(sign + monSplitted[0]);
//				if (sign == "-") {
//					coef = coef*(-1);
//				}
				//System.out.println(coef);
				Integer grad = (monSplitted.length == 1) ? 0 : Integer.parseInt(monSplitted[monSplitted.length -1]);
				Monom monom = new MonomIntreg(coef, grad);
				//System.out.println(grad);
				System.out.println(monom.toString());
			}
			
			//}
//			System.out.println(spited[i]);
//			if (i>0){
//			String before = pol.substring(pol.indexOf(spited[i])-1, pol.indexOf(spited[i]));
//			System.out.println(before);
//			}
			System.out.println("*****");
			
		}
		
	}

}
