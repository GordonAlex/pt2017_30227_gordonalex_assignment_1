package polinom;
import monom.Monom;

public class Polinom {

	protected Monom[] monoame = new Monom[100];
 
	public Polinom() {
		
	}
	
	public void setMonoame(Monom[] m) {
		this.monoame = m;
	}
	
	public Monom[] getMonoame()
	{
		return this.monoame;
	}
	
	public void addMonom(Monom m) {
		Monom existingMonom = m;
		
		if (this.monoame[m.getGrad()] != null) {
			existingMonom = this.monoame[m.getGrad()];
			existingMonom.addCoefConst(m.getCoef());
		}
				
		this.monoame[m.getGrad()] = existingMonom;
	}
	
	public Integer getGrad() {
		Integer grad = 0;
		
		for (Monom monom : this.monoame) {
			if (monom != null && monom.getGrad() > grad && monom.getCoef().intValue() !=0) {
				grad = monom.getGrad();
			}
		}
		
		return grad;
	}
	
	public Monom getMonomFromPos(Integer pozitie) {
		if (this.monoame[pozitie] != null) {
			return this.monoame[pozitie];
		}
		
		return null;
	}
	
	public String toString() {
		String s = "";
		
		for (Monom monom : this.monoame) {
			if (monom != null) {
				s = s + monom.toString();
			}			
		}
		return s;
	}
	
}
