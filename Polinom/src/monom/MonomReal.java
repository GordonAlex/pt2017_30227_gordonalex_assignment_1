package monom;
public class MonomReal extends Monom {

	public MonomReal(Double coef, Integer grad) {
		super(coef, grad);
	}
	
	@Override
	public Number getCoef() {
		return this.getCoef().doubleValue();
	}
	
	@Override
	public void addCoefConst(Number value) {
		Number newCoef = this.getCoef().doubleValue() + value.doubleValue();
		this.setCoef(newCoef);
	}

	@Override
	public MonomReal mulCoefConst(Number value) {
		Number newCoef = this.getCoef().doubleValue() * value.doubleValue();
		this.setCoef(newCoef);	
		
		return this;
	}
	
	public MonomReal divCoefConst(Number value) {
		Number newCoef = this.getCoef().doubleValue() / value.doubleValue();
		this.setCoef(newCoef);	
		
		return this;
	}
	
	public Monom addMultipliedValue(Number coef, Integer grad) {
		Number newCoef = this.getCoef().doubleValue() * coef.doubleValue();
		Integer newGrad = this.getGrad() + grad;
		
		this.setCoef(newCoef);
		this.setGrad(newGrad);
		
		return this;
	}
	
	@Override
	public Monom oposite() {
		Number newCoef = this.getCoef().doubleValue() * (-1);
		this.setCoef(newCoef);
		
		return this;
	}
	
	@Override
	public String getCoefSign() {
		if (this.getCoef().doubleValue() < 0) {
			return "";
		}
		
		return "+";
	}

	@Override
	public MonomReal addDividedValue(Number coef, Integer grad) {
		Number newCoef = this.getCoef().doubleValue() / coef.doubleValue();
		this.setCoef(newCoef);
		
		Integer newGrad = this.getGrad().intValue() - grad.intValue();
		this.setGrad(newGrad);
		
		return this;
		
	}
	
}
